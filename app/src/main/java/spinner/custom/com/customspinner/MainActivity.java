package spinner.custom.com.customspinner;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Spinner;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    CustomSpinnerAdapter customSpinnerAdapter;
    Spinner spinner;
    TextView resultTextView;
    String result = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initViews();
        initSpinner();
    }


    private void initViews() {
        spinner = (Spinner)findViewById(R.id.spinner);
        resultTextView = (TextView)findViewById(R.id.resultText);
    }

    private void initSpinner() {
        CustomSpinnerAdapter<String> monthSpinner = new CustomSpinnerAdapter<>(this,
                android.R.layout.simple_spinner_dropdown_item,
                getResources().getStringArray(R.array.grad_months));

        spinner.setAdapter(monthSpinner);

        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                result = spinner.getItemAtPosition(position).toString();
                resultTextView.setText(result);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }
}
